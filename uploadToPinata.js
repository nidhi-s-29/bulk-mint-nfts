//imports needed for this function
const axios             = require ( 'axios' );
const fs                = require ( 'fs' );
const FormData          = require ( 'form-data' );
const recursive         = require ( 'recursive-fs' );
const basePathConverter = require ( 'base-path-converter' );

const API_Key    = "4d2ed681737a46adeef7";
const API_Secret = "1b106af9a0489880170b813779891f0a6b7ebe41b86037es4f79dca43d6d99df";
const JWT        = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2vsySW5mb3JtYXRpb24iOnsiaWQfsOiJiYjI0M2U4Zi02MDM0LTQzMmUtYjhmMS00MjRmODE2MmUyZmYiLCJlbWFpbCI6Im5vb2JjaGFuLm5mdEBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwicGluX3BvbGljeSI6eyJyZWdpb25zIjpbeyJpZCI6IkZSQTEiLCJkZXNpcmVkUmVwbGljYXRpb25Db3VudCI6MX1dLCJ2ZXJzaW9uIjoxfSwibWZhX2VuYWJsZWQiOmZhbHNlfSwiYXV0aGVudGljYXRpb25UeXBlIjoic2NvcGVkS2V5Iiwic2NvcGVkS2V5S2V5IjoiNGQyZWQ2ODE3MzdhNDZhNjZlZDciLCJzY29wZWRLZXlTZWNyZXQiOiIxYjEwNmFmOWEwNDg5ODgwMTcwYjgxMzc3OTg5MWYwYTZiN2ViZTQxYjg2MDM3ZWE0Zjc5MWNhNDNkNmQ5OWRmIiwiaWF0IjoxNjQ3MDE5MTQ0fQ.As9ynj7hVWqGLaL2pccOXPKwwtsbwDyNLj1MMRdcDZA"

const pinDirectoryToIPFS = async () => {
	const url = `https://api.pinata.cloud/pinning/pinFileToIPFS`;
	const src = './images';
	
	//we gather the files from a local directory in this example, but a valid readStream is all that's needed for each file in the directory.
	return recursive.read ( src ).then ( res => {
		let { files } = res;
		let data      = new FormData ({ maxDataSize:  6442450944 });
		files.forEach ( ( file ) => {
			//for each file stream, we need to include the correct relative file path
			data.append ( `file`, fs.createReadStream ( file ), {
				filepath : basePathConverter ( src, file )
			} );
			console.log(file);
		} );
		
		const metadata = JSON.stringify ( {
			name : 'images'
		} );
		data.append ( 'pinataMetadata', metadata );
		
		return axios
			.post ( url, data, {
				maxBodyLength : Infinity, //this is needed to prevent axios from erroring out with large directories
				headers       : {
					'Content-Type'        : `multipart/form-data; boundary=${ data._boundary }`,
					pinata_api_key        : API_Key,
					pinata_secret_api_key : API_Secret
				}
			} )
			.then ( function ( response ) {
				console.log("axios response");
				console.log ( response.data );
			} )
			.catch ( function ( error ) {
				//handle error here
				console.log("axios error");
				console.error ( error );
			} );
	} ).catch ( function ( error ) {
		//handle error here
		console.error ( "Reading error" );
		console.error ( error );
	} );
	
};

pinDirectoryToIPFS ().then ( () => {
} ).catch ( err => {
	console.error ( "function error" );
	console.error ( err );
} );

    
